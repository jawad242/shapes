<?php
    require_once '../vendor/autoload.php';

    // Namespaces of the shapes in use
    use jhumayun\Shapes\shapes\Circle;
    use jhumayun\Shapes\shapes\Square;

    try{
        $circle_params = array(
                                'r'=>'10'
                            );
        $Circle = new Circle($circle_params);
        echo "<pre>Parameters of Circle: ".print_r($Circle->getParams() ,1)."</pre>";
        echo "<pre>Radius = 10</pre>";
        echo "<pre>Circle Perimeter: ".print_r($Circle->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Circle Area: ".print_r($Circle->calculateArea() ,1)."</pre>";

        $Circle->setParam('r',2);
        echo "<pre>Radius = 2</pre>";
        echo "<pre>Circle Perimeter: ".print_r($Circle->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Circle Area: ".print_r($Circle->calculateArea() ,1)."</pre>";
        
        $square_params = array(
            's'=>'1'
        );
        $Square = new Square($square_params);
        echo "<pre>Parameters of Square: ".print_r($Square->getParams() ,1)."</pre>";
        echo "<pre>Side = 1</pre>";
        echo "<pre>Square Perimeter: ".print_r($Square->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Square Area: ".print_r($Square->calculateArea() ,1)."</pre>";
    }
    catch(\Exception $e){
        die('Caught exception '.$e->getMessage());
    }
?>