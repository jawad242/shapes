# Shapes

Library of shapes to calculate their area and perimeter.

### Composer install command

```cod
composer require jhumayun/shapes
```

### Usage

The shapes class can be loaded in two ways

1. **Method 1**: using namespace of the class
2. **Method 2**: using ShapeFactory object (Recommended)

#### Loading shape using namespace

```php
<?php
    // Require Autoloader
    require_once '../vendor/autoload.php';

    // Namespaces of the shapes in use
    use jhumayun\Shapes\shapes\Circle;
    use jhumayun\Shapes\shapes\Square;

    try{
        $circle_params = array(
                                'r'=>'10'
                            );
        $Circle = new Circle($circle_params);
        echo "<pre>Parameters of Circle: ".print_r($Circle->getParams() ,1)."</pre>";
        echo "<pre>Radius = 10</pre>";
        echo "<pre>Circle Perimeter: ".print_r($Circle->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Circle Area: ".print_r($Circle->calculateArea() ,1)."</pre>";

        $Circle->setParam('r',2);
        echo "<pre>Radius = 2</pre>";
        echo "<pre>Circle Perimeter: ".print_r($Circle->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Circle Area: ".print_r($Circle->calculateArea() ,1)."</pre>";
        
        $square_params = array(
            's'=>'1'
        );
        $Square = new Square($square_params);
        echo "<pre>Parameters of Square: ".print_r($Square->getParams() ,1)."</pre>";
        echo "<pre>Side = 1</pre>";
        echo "<pre>Square Perimeter: ".print_r($Square->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Square Area: ".print_r($Square->calculateArea() ,1)."</pre>";
    }
    catch(\Exception $e){
        die('Caught exception '.$e->getMessage());
    }
?>
```

#### Loading Shape using ShapeFactory Class

```php
<?php
    require_once '../vendor/autoload.php';
    
    use jhumayun\Shapes\core\ShapesFactory;

    try{
        $factory = new ShapesFactory();
        
        $circle_params = array(
            'r'=>'10'
        );
        $Circle = $factory::create('Circle'); // instentiating without parameters
        echo "<pre>Parameters of Circle: ".print_r($Circle->getParams() ,1)."</pre>";

        $Circle = $factory::create('Circle', $circle_params); // instentiating with parameters
        echo "<pre>Radius = 10</pre>";
        echo "<pre>Circle Perimeter: ".print_r($Circle->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Circle Area: ".print_r($Circle->calculateArea() ,1)."</pre>";

        $Circle->setParam('r',2);
        echo "<pre>Radius = 2</pre>";
        echo "<pre>Circle Perimeter: ".print_r($Circle->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Circle Area: ".print_r($Circle->calculateArea() ,1)."</pre>";

        $square_params = array(
        's'=>'1'
        );
        $Square = $factory::create('Square', $square_params);
        echo "<pre>Parameters of Square: ".print_r($Square->getParams() ,1)."</pre>";
        echo "<pre>Side = 1</pre>";
        echo "<pre>Square Perimeter: ".print_r($Square->calculatePerimeter() ,1)."</pre>";
        echo "<pre>Square Area: ".print_r($Square->calculateArea() ,1)."</pre>";
    }
    catch(\Exception $e){
        die('Caught exception '.$e->getMessage());
    }
?>
```



### Method Description

| Method             | Description                                | Return Type |
| ------------------ | ------------------------------------------ | ----------- |
| getName            | get Name of the shape                      | string      |
| getDimensions      | get dimensions of the shape                | numeric     |
| getParams          | get parameter structure of the shape       | array       |
| setParam           | set value of single parameter of the shape | void        |
| calculatePerimeter | calculate Perimeter of the shape           | numeric     |
| calculateArea      | calculate Area of the shape                | numeric     |



### Adding a new Shape

Each shape *extends* **ShapesBase** Class which is an *abstract* class, and the **ShapesBase** *implements* **ShapesTemplate**. All the core functions are already defined in **ShapesBase** Class therefore new shape will need to define only **constructor**, **calculatePerimeter** method, **calculateArea** method. 

Lets say, name of your new shape is **myShape** then you need to add a file **myShape.php** into 'src\shapes' directory as following.

```php
<?php

namespace jhumayun\Shapes\shapes;

use jhumayun\Shapes\core\ShapesBase;

class myShape extends ShapesBase{

    protected $Name = 'myShape';

    protected $Dimensions = '2';

    protected $valid_params = array(
        'p1' => 'where p1 is the perameter 1 of myShape',
        'p2' => 'where p1 is the perameter 2 of myShape'
    );

    public function __construct($params){
        parent::__construct($this->Name, $this->Dimensions, $params);
    }

    public function calculatePerimeter(){
        // assume that formula of Perimeter of myShape is p1+p2
        $res = floatval($this->Params['p1']['value']) + floatval($this->Params['p2']['value']);
        return number_format($res,2,'.','');
    }

    public function calculateArea(){
        // assume that formula of Perimeter of myShape is p1*p2
        $res = floatval($this->Params['p1']['value']) * floatval($this->Params['p2']['value']);
        return number_format($res,2,'.','');
    }
}
```



