<?php

namespace jhumayun\Shapes\tests;
use jhumayun\Shapes\shapes\Square;

class SquareTest extends \PHPUnit_Framework_TestCase
{
    private $shape;

    private function getParams(){
        $params = array(
            's'=>'1'
        );
        return $params;
    }

    private function initShape(){
        $this->shape = new Square($this->getParams());
    }

    public function testExtendsShapesBase(){
        $flg = false;
        $parent = get_parent_class('jhumayun\Shapes\shapes\Square');
        $this->assertEquals('jhumayun\Shapes\core\ShapesBase', $parent);
    }

    public function testgetName(){
        $this->initShape();
        $this->assertEquals('Square', $this->shape->getName());
    }

    public function testgetDimensions(){
        $this->initShape();
        $this->assertEquals('2', $this->shape->getDimensions());
    }

    public function testPerimeterCalculation(){
        $this->initShape();
        $val = $this->shape->calculatePerimeter();
        $this->assertEquals('4.00', $val);
    }

    public function testAreaCalculation(){
        $this->initShape();
        $val = $this->shape->calculateArea();
        $this->assertEquals('1.00', $val);
    }

    public function testsetParam(){
        $this->initShape();
        $this->shape->setParam('s', 5);
        $this->assertEquals('20', $this->shape->calculatePerimeter());
    }
}