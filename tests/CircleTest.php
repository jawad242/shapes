<?php

namespace jhumayun\Shapes\tests;
use jhumayun\Shapes\shapes\Circle;

class CircleTest extends \PHPUnit_Framework_TestCase
{
    private $shape;

    private function getParams(){
        $params = array(
            'r'=>'10'
        );
        return $params;
    }

    private function initShape(){
        $this->shape = new Circle($this->getParams());
    }

    public function testExtendsShapesBase(){
        $flg = false;
        $parent = get_parent_class('jhumayun\Shapes\shapes\Circle');
        $this->assertEquals('jhumayun\Shapes\core\ShapesBase', $parent);
    }

    public function testgetName(){
        $this->initShape();
        $this->assertEquals('Circle', $this->shape->getName());
    }

    public function testgetDimensions(){
        $this->initShape();
        $this->assertEquals('2', $this->shape->getDimensions());
    }

    public function testPerimeterCalculation(){
        $this->initShape();
        $val = $this->shape->calculatePerimeter();
        $this->assertEquals('62.83', $val);
    }

    public function testAreaCalculation(){
        $this->initShape();
        $val = $this->shape->calculateArea();
        $this->assertEquals('314.16', $val);
    }

    public function testsetParam(){
        $this->initShape();
        $this->shape->setParam('r', 2);
        $this->assertEquals('12.57', $this->shape->calculatePerimeter());
    }
}