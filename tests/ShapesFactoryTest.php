<?php

namespace jhumayun\Shapes\tests;
use jhumayun\Shapes\core\ShapesFactory;

class ShapesFactoryTest extends \PHPUnit_Framework_TestCase
{
    private $shape;

    private function getParams(){
        $circle_params = array(
            'r'=>'10'
        );
        return $circle_params;
    }

    private function initShape(){
        $factory = new ShapesFactory();
        $this->shape = $factory::create('Circle', $this->getParams());
    }

    public function testPerimeterCalculation(){
        $this->initShape();
        $val = $this->shape->calculatePerimeter();
        $this->assertEquals('62.83', $val);
    }

    public function testAreaCalculation(){
        $this->initShape();
        $val = $this->shape->calculateArea();
        $this->assertEquals('314.16', $val);
    }

    public function testUnknownShapeException(){
        $exception = false;
        try{
            $factory = new ShapesFactory();
            $this->shape = $factory::create('UnknownShape', $this->getParams());
        }
        catch(\Exception $e){
            $exception = true;
        }
        $this->assertEquals(true, $exception);
    }
}

?>