<?php

namespace jhumayun\Shapes\shapes;

use jhumayun\Shapes\core\ShapesBase;

class Square extends ShapesBase{

    protected $Name = 'Square';

    protected $Dimensions = '2';

    protected $valid_params = array(
        's' => 'where s is the length of each side of square'
    );

    public function __construct($params){
        parent::__construct($this->Name, $this->Dimensions, $params);
    }

    public function calculatePerimeter(){
        $res = 4*floatval($this->Params['s']['value']);
        return number_format($res,2,'.','');
    }

    public function calculateArea(){
        $res = floatval($this->Params['s']['value'])*floatval($this->Params['s']['value']);
        return number_format($res,2,'.','');
    }
}