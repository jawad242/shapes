<?php

namespace jhumayun\Shapes\shapes;

use jhumayun\Shapes\core\ShapesBase;

class Circle extends ShapesBase{

    protected $Name = 'Circle';

    protected $Dimensions = '2';

    protected $valid_params = array(
        'r' => 'where r is the radius of circle'
    );

    public function __construct($params){
        parent::__construct($this->Name, $this->Dimensions, $params);
    }

    public function calculatePerimeter(){
        $res = 2*pi()*floatval($this->Params['r']['value']);
        return number_format($res,2,'.','');
    }

    public function calculateArea(){
        $res = pi()*floatval($this->Params['r']['value'])*floatval($this->Params['r']['value']);
        return number_format($res,2,'.','');
    }
}