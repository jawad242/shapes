<?php

namespace jhumayun\Shapes\core;

use jhumayun\Shapes\core\ShapesTemplate;

abstract class ShapesBase implements ShapesTemplate{

    protected $Name;

    protected $Dimensions;

    protected $Params = array();

    // Force Extending class to define this method
    abstract public function calculatePerimeter();
    abstract public function calculateArea();

    public function __construct($name, $dim, $params = array()){
        $this->Name = $name;
        $this->Dimensions = $dim;
        if(!empty($params)){
            $this->validateParamsArrayStructure($params);
        }
    }

    protected function validateParamsArrayStructure($params = array()){
        if(empty($params)){
            $params = $this->Params;
        }

        foreach($this->valid_params as $pName=>$pDesc){
            if(isset($params[$pName])){
                $this->Params[$pName] = array(
                    'value' => $params[$pName],
                    'description' => $pDesc
                );
            }
            else{
                $message = "Required parameter '$pName' is missing for the shape ".$this->getName();
                throw new \Exception($message);
            }
        }
    }

    public function getName(){
        return $this->Name;
    }

    public function getDimensions(){
        return $this->Dimensions;
    }

    public function getParams(){
        return $this->valid_params;
    }

    public function setParam($ParamName, $value){
        $this->validateParamsArrayStructure($this->Params);
        if(!isset($this->Params[$ParamName])){
            $message = "No parameter defined with name '".$ParamName."' for the shape '".$this->getName()."'";
            throw new \Exception($message);
        }

        if(!is_numeric($value)){
            $message = "The value of parameter '".$ParamName."' must be numeric. Value passed '".$value."'";
            throw new \Exception($message);
        }

        return $this->Params[$ParamName]['value'] = $value;
    }

}