<?php
namespace jhumayun\Shapes\core;

interface ShapesTemplate
{
    public function getName();

    public function getDimensions();

    public function getParams();

    public function setParam($ParamName, $value);

    public function calculatePerimeter();

    public function calculateArea();
}