<?php

namespace jhumayun\Shapes\core;

class ShapesFactory{
    public static function create($shape, $params = array())
    {
        $class = "\jhumayun\Shapes\shapes\\$shape";
        if(class_exists($class)){
            return new $class($params);
        }
        else{
            $msg = 'class "'.$class.'" Does not exist';
            throw new \Exception($msg);
        }
    }
}